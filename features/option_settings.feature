Feature: Option Settings
  Use the options in the Settings tab of the DataDive Research Sheet for result personalization


# Basic start background scenario
  Background:
    Given I am on my DataDive generated research sheet for "<product>"
    And the Master KW List is filled


# Scenario to check lowering Maximum Relevant Rank effect on Master KW tab, less relevant products expected
  Scenario Outline: Lowering Maximum Relevant Rank effects Relevancy
    Given the first keyword row has a Relevancy of <rlv>
    When I scroll the tabs till Settings is visible
    And I click on the Settings tab
    Then the Settings sheet is filled
    Then the Maximum Relevant Rank has a value of <mrr>
    When I clear the Maximum Relevant Rank cell
    And I enter a value of <mrr2> to the Maximum Relevant Rank cell
    And I click on the CLICK HERE TO APPLY settings and Master KW List filters button
    And I scroll the tabs till Master KW List is visible
    And I click on the Master KW List tab
    Then Relevancy of the first keyword row must be <rlv2>

  Examples:
    | product                       | mrr   | mrr2  | rlv  | rlv2 |
    | Mechanical Gaming Keyboard    | 45    | 20    | 8    | 6    |
    | Wired Gaming Mouse            | 50    | 10    | 6    | 2    |


# Scenario to check Maximum Competitor Quality Treshold Percentage, lowering green value
  Scenario Outline: Green - Yellow Treshold Percentage change
    Given the third product has a % of SV on P1 value of <green> and is colored green
    When I scroll the tabs till Settings is visible
    And I click on the Settings tab
    Then the Settings sheet is filled
    Then Maximum Competitor Quality treshold percentage for green has a value of <green>
    When I clear the Maximum Competitor Quality for green treshold percentage for green cell
    And I enter a value of <green2> to the Maximum Competitor Quality treshold percentage for green
    And I click on the CLICK HERE TO APPLY settings and Master KW List filters button
    And I scroll the tabs till Master KW List is visible
    And I click on the Master KW List tab
    Then the third product should be colored yellow
    
  Examples:
    | product                       | green   | green2   |
    | Mechanical Gaming Keyboard    | 40%     | 20%      |
    | Wired Gaming Mouse            | 30%     | 15%      |


# Scenario to check Maximum Competitor Quality Treshold Percentage, raising orange
  Scenario Outline: Orange - Red Treshold Percentage change
    Given the first product has a % of SV on P1 value of <orange2> and is colored red
    When I scroll the tabs till Settings is visible
    And I click on the Settings tab
    Then the Settings sheet is filled
    Then Maximum Competitor Quality treshold percentage for orange has a value of <orange>
    When I clear the Maximum Competitor Quality treshold percentage for orange
    And I enter a value of <orange2> to the Maximum Competitor Quality treshold percentage for orange
    And I click on the CLICK HERE TO APPLY settings and Master KW List filters button
    And I scroll the tabs till Master KW List is visible
    And I click on the Master KW List tab
    Then the first product should be colored orange
    
  Examples:
    | product                       | orange   | orange2   |
    | Mechanical Gaming Keyboard    | 80%      | 90%       |
    | Wired Gaming Mouse            | 65%      | 75%       |


# Scenario to remove a treshold color
  Scenario Outline: Same Treshold Percentage effect
    Given there are products colored yellow
    When I scroll the tabs till Settings is visible
    And I click on the Settings tab
    Then Maximum Competitor Quality treshold percentage for yellow has a value of <yellow>
    Then Maximum Competitor Quality treshold percentage for green has a value of <green>
    When I clear the Maximum Competitor Quality treshold percentage for yellow
    And I enter a value of <green> to the Maximum Competitor Quality treshold percentage for yellow
    And I click on the CLICK HERE TO APPLY settings and Master KW List filters button
    And I scroll the tabs till Master KW List is visible
    And I click on the Master KW List tab
    Then none of the products should be colored yellow
    
  Examples:
    | product                       | yellow  | green  |
    | Mechanical Gaming Keyboard    | 60%     | 40%    |
    | Wired Gaming Mouse            | 50%     | 25%    |
