Feature: Your Strategy
  Calculate products per order using the Your Strategy section of the Profitability Calculator tool


# Basic start background scenario
  Background:
    Given I am on the website of Amazon
    When I click on Deliver to menu
    And I enter â90017â in the my US zip code textbox
    * I click the apply button
    * I click the Done button
    Then the Deliver menu to is changed to âLos Angeles 90017â in the Amazon Page
    When I enter Mechanical Gaming Keyboard in the search bar
    And I click on the Search button
    Then the results for Mechanical Gaming Keyboard page is shown
    When I click on the first result
    Then the product page is shown
    Then the DataDive Profitability Calculator button is enabled
    When I click the DataDive Profitability Calculator button


# Failure Scenario
  Scenario: Calculate products per order without monthly sales velocity
    Then I am on the DataDive Mechanical Gaming Keyboard Profits Page
    When I clear the Monthly sales velocity textbox value
    Then the Products per Order textbox should be populated with a value of âpcs/order
    And a warning to populate the Monthly sales velocity textbox should be shown


# Base Scenario
  Scenario Outline: Calculate products per order
    Then I am on the DataDive Mechanical Gaming Keyboard Profits Page
    When I enter a value of <monthly-sales-velocity> in the Monthly sales velocity textbox
    Then the Products per Order textbox should be populated with a value of <products-per-order>
    
  Examples:
    | monthly-sales-velocity | products-per-order |
    | 500                    | 1760               |
    | 850                    | 2976               |


# Longer production time scenario 
  Scenario Outline: Calculate longer production time effect on products per order
    Then I am on the DataDive Mechanical Gaming Keyboard Profits Page
    And the Production lead time field is populated with a value of 2 weeks
    When I write a value of <monthly-sales-velocity> in the Monthly sales velocity textbox
    And the Products per Order textbox is populated with a value of <products-per-order>
    * I select a Production lead time value of 4 weeks from the drop down menu
    Then the Products per Order textbox should be populated with a value of <products-per-order2>

  Examples:
    | monthly-sales-velocity | products-per-order | products-per-order2 |
    | 500                    | 1760               | 1984                |
    | 850                    | 2976               | 3392                |


# Higher monthly sales scenario 
  Scenario Outline: Calculate higher monthly sales effect on products per order
    Then I am on the DataDive Mechanical Gaming Keyboard Profits Page
    When I write a value of <monthly-sales-velocity> in the Monthly sales velocity textbox
    Then the Products per Order textbox is populated with a value of <products-per-order>
    When I write a value of <monthly-sales-velocity2> in the Monthly sales velocity textbox
    Then the Products per Order textbox should be populated with a value of <products-per-order2>

  Examples:
    | monthly-sales-velocity | products-per-order | monthly-sales-velocity2 | products-per-order2 |
    | 500                    | 1760               | 850                     | 2976                |
    | 850                    | 2976               | 1000                    | 3968                |
