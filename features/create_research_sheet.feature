Feature: Create research sheet for product
  Generate a research sheet to study a product's presence in Amazon


# Basic start background scenario
  Background:
    Given I am on the website of Amazon
    When I click on Deliver to menu
    And I enter â90017â in the my US zip code textbox
    * I click the apply button
    * I click the Done button
    Then the Deliver menu to is changed to âLos Angeles 90017â in the Amazon Page
    When I enter "<product>" in the search bar
    And I click on the Search button


# Success case 
  Scenario Outline: Generate best seller sheet of "<product>" on Amazon
    Then the results page for "<product>" is shown
    When I click on the Helium10 extension
    And I click on the XRay option
    Then a table with results of products with "<product>" in the title is shown
    When I double click the Sales column header
    Then the results are sorted by sales
    When I select <number> items with relevant title and image
    And I click on the Dive button
    Then a Gsheet file should be generated
    And the generated file name should contain the date in YYMMDD format, <account_file_number> and "<product>"
    And <number> products should be shown on the product table

  Examples:
    | product         | number    | account_file_number |
    | dog hat         | 5         | 22                  |
    | scented candle  | 10        | 44                  |
    | baby tumbler    | 20        | 32                  |


# Failure case
  Scenario: Generate best seller sheet of product on Amazon without a subscription
    Then the results page for dog hat is shown
    But my DataDive subscription has ended
    When I click on the Helium10 extension
    And I click on the XRay option
    Then a table with results of products with dog hat in the title is shown
    * I select Research the top 10 results from the DataDive dropdown menu
    * I click on the Dive button
    Then a warning to activate a subscription to DataDive should be shown
