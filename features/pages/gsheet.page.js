const BasePage = require('./base.page');
class GsheetPage extends BasePage {
    get fileTitle () { return $('#docs-title-input-label-inner').getText() }
}
module.exports = new GsheetPage();
