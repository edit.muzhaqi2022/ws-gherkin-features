class HeliumPage {
    get heliumTitle () { return $('.modalDragHandle')[0].text() }
    get asinsCount () { return $('.asins-count-select')[0] }
    get salesHeader () { return $('.sc-cNuihh ggSrOn')[1] }
    get salesFirstCell () { return $('.sc-ddDeZa kMotsz')[1].text() }
    get salesSecondCell () { return $('.sc-ddDeZa kMotsz')[31].text() }

}
module.exports = new HeliumPage();
