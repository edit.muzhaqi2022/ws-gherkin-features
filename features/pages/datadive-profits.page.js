const BasePage = require('./base.page');
class DataDiveProfitsPage extends BasePage {
    get dataDiveButton () { return $('.profitability-button primary')[0] }
    get monthlySalesTextbox () { return $('#your_strategy_monthly_sales_velocity') }
    get DataDiveProfitsLink () { return 'chrome-extension://mnjclpmbklnceaoeochkhckaphjhnpib/assets/profits.html' }
    get dataDiveProductTitle () { return $('.d-flex align-items-center lead col')[0].text() }
    get msvAlert () { return $('.alert alert-warning py-0 m-0 w-100')[0] }
    get productsPerOrder () { return $('#your_strategy_products_per_order').text() }
    get prodLeadTime () { return $('#your_strategy_production_lead_time_weeks').find(":selected").text() }
    get prodLeadTimeMenu () { return $('#your_strategy_production_lead_time_weeks') }
    get subscribeButton () { return $('.button primary consent-submit')[0].text() }
    get diveButton () { return $('.research-competitors-button primary')[0] }
    get yourStrategyOpen () { return $('#react-collapsed-toggle-1').getAttribute('aria-expanded') }
    get productDesignButton () { return $('#react-collapsed-toggle-2') }
    get productDesignOpen () { return $('#react-collapsed-toggle-2').getAttribute('aria-expanded') }
    get asinsCount () { return $('.asins-count-select')[0] }
    get referralFeeDescription () { return $('#product_design_product_category_sales_commission_rule_description').text() }
    get bestShippingProfit () { return $('#bottom_line_oceanfclamz_product_gross_profit').text() }
    get productWeightFieldValue () { return $('#product_design_product_box_weight').getValue() }
    get productWeightField () { return $('#product_design_product_box_weight') }
    get categoryMenu () { return $('#product_design_product_category') }
    get category () { return $('#product_design_product_category').find(":selected").text() }

}
module.exports = new DataDiveProfitsPage();
