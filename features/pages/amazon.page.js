const BasePage = require('./base.page');
class AmazonPage extends BasePage {
    get deliverToButton () { return $('#nav-global-location-popover-link') }
    get zipTextbox () { return $('#GLUXZipUpdateInput') }
    get zipApplyButton () { return $('.a-button-input')[0] }
    get zipDoneButton () { return $('button[type="button"]') }
    get deliverToText () { return $('#glow-ingress-line2') }
    get searchBar () { return $('#twotabsearchtextbox') }
    get searchButton () { return $('#nav-search-submit-button') }
    get dataDiveButton () { return $('.profitability-button primary')[0] }
    get monthlySalesTextbox () { return $('#your_strategy_monthly_sales_velocity') }
    get resultsPage () { return $('.a-section a-spacing-small a-spacing-top-small')[0].text() }
    get firstResultLink () { return $('.a-link-normal s-link-style a-text-normal')[0] }
    get productTitle () { return $('#productTitle').text() }

    open() {
        super.open('https://www.amazon.com/')
    }
}
module.exports = new AmazonPage();
