Feature: Category Referral Fee
  Calculate referral fee by product category in DataDive Profitability Calculator for Amazon


# Basic start background scenario
  Background:
    Given I am on the website of Amazon
    When I click on Deliver to menu
    And I enter â90017â in the my US zip code textbox
    * I click the apply button
    * I click the Done button
    Then the Deliver menu to is changed to âLos Angeles 90017â in the Amazon Page
    When I enter Mechanical Gaming Keyboard in the search bar
    And I click on the Search button
    Then the results page for Mechanical Gaming Keyboard is shown
    When I click on the first result
    Then the product page is shown
    Then the DataDive Profitability Calculator button is enabled
    When I click the DataDive Profitability Calculator button
    Then the DataDive Mechanical Gaming Keyboard Profits page is shown
    Then the Your Strategy section is visible
    When I enter 100 in the monthly sales velocity textbox
    And I click on the Product Design section


# Scenario to test profit change, knowing Amazon Device Accessories category has the highest referral fee: 45%, minimum $0.30
  Scenario Outline: Profit reduction according to higher referral fee category
    Then the Product Design section is visible
    And the Category field has a value of "<category>"
    * the Product Gross Profit for Best shipping method has a value of <profit>
    When I select Amazon Device Accessories in the Category dropdown menu
    Then Product Gross Profit column for Best shipping method must be equal to <profit2>
    
  Examples:
    | category               | profit  | profit2  |
    | Consumer Electronics   | $5.14   | $3.16    |
    | Electronics Accesories | $11.17  | $8.77    |


# Scenario to test minimum referral fee price remains the same as long as the minimum is not surpassed
  Scenario Outline: Calculate referral fee using minimum value
    Then the Product Design section is visible
    And the Referral fee text has a value of <referral_fee> for the default category with minimum of <min>
    * the Product Gross Profit for Best shipping method has a value of <profit>
    * the Product Weight has a value of <pw>
    * the sum of <price> and <fba_fee> times the <referral_fee> is lower than or equal to <min>
    When I clear the Product Weight textbox
    And I enter <pw2> in the Product Weight textbox
    Then value of Product Gross Profit for Best shipping method must remain <profit>

  Examples:
    | referral_fee   | min     | price  | fba_fee  | profit | pw  | pw2  |
    | 8%             | $0.30   | $0.1   | $2.92    | $5.14  | 1   | 0.5  |
    | 15%            | $1      | $1.1   | $2.84    | $11.17 | 2   | 1    |


# Scenario to test referral fee change within the same category. eg: Grocery has 8% up to $15, 15% otherwise.
  Scenario Outline: Calculate referral fee percentage change within category
    Then the Product Design section is visible
    And the selected category has a referral fee text of <referral_fee_1> up to <min>, <referral_fee_2> otherwise
    * the Product Gross Profit for Best shipping method has a value of <profit>
    * the Product Weight has a value of <pw>
    * the sum of <price> and <fba_fee> times the <referral_fee> is equal to <min>
    When I clear the Product Weight textbox
    And I enter <pw2> in the Product Weight textbox
    Then value of Product Gross Profit for Best shipping method must be <profit2>

  Examples:
    | referral_fee_1  | min     | referral_fee_2  | price  | fba_fee  | profit | profit2 | pw  | pw2 |
    | 8%              | $15     | 15%             | $0.1   | $2.92    | $5.14  | $3.17   | 0.5 | 1   |
    | 15%             | $20     | 20%             | $1.1   | $2.84    | $11.17 | $7.49   | 1   | 2   |