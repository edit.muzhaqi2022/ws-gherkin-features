const { Given, When, Then } = require('@cucumber/cucumber');
const AmazonPage = require('../pages/amazon.page');
const DataDiveProfitsPage = require('../pages/datadive-profits.page');


// 1) Scenario: Profit reduction according to higher referral fee category # features\category_referral_fee.feature:27

  Given('I am on the website of Amazon', function () {
    AmazonPage.open();
    browser.maximizeWindow();
    expect(browser).toHaveTitleContaining('Amazon');
  });

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter Mechanical Gaming Keyboard in the search bar', function () {
    AmazonPage.searchBar.setValue('Mechanical Gaming Keyboard');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for Mechanical Gaming Keyboard is shown', function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I click on the first result', function () {
    AmazonPage.firstResultLink.click();
  });

  Then('the product page is shown', function () {
    expect(AmazonPage.productTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the DataDive Profitability Calculator button is enabled', function () {
    expect(DataDiveProfitsPage.dataDiveButton).toExist();
  });

  When('I click the DataDive Profitability Calculator button', function () {
    DataDiveProfitsPage.dataDiveButton.click();
  });  
    
  Then('the DataDive Mechanical Gaming Keyboard Profits Page is visible', function () {
    expect(browser).toHaveLinkContaining(DataDiveProfitsPage.DataDiveProfitsLink);
    expect(DataDiveProfitsPage.dataDiveProductTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the Your Strategy section is visible'), function () {
    expect(DataDiveProfitsPage.yourStrategyOpen).toHaveValueContaining('true');
  };

  When('I enter 100 in the monthly sales velocity textbox'), function () {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(100);
  };

  When('I click on the Product Design section'), function () {
    DataDiveProfitsPage.productDesignButton.click();
  };

  Then('the Product Design section is visible'), function () {
    expect(DataDiveProfitsPage.productDesignOpen).toHaveValueContaining('true');
  };

  Then('the Category field has a value of {category}'), function (category) {
    expect(DataDiveProfitsPage.category).toHaveTextContaining(category);
  };

  Then('the Product Gross Profit for Best shipping method has a value of {profit}'), function (profit) {
    expect(DataDiveProfitsPage.bestShippingProfit).toHaveTextContaining(profit);
  };

  When('I select Amazon Device Accessories in the Category dropdown menu'), function () {
    DataDiveProfitsPage.categoryMenu.selectByAttribute('value', 'amazonDeviceAccessories');
  };

  Then('Product Gross Profit column for Best shipping method must be equal to {profit2}'), function (profit2) {
    expect(DataDiveProfitsPage.bestShippingProfit).toHaveTextContaining(profit2);
  };


// 2) Scenario: Calculate referral fee using minimum value # features\category_referral_fee.feature:41

  Given('I am on the website of Amazon', function () {
    AmazonPage.open();
    browser.maximizeWindow();
    expect(browser).toHaveTitleContaining('Amazon');
  });

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter Mechanical Gaming Keyboard in the search bar', function () {
    AmazonPage.searchBar.setValue('Mechanical Gaming Keyboard');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for Mechanical Gaming Keyboard is shown', function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I click on the first result', function () {
    AmazonPage.firstResultLink.click();
  });

  Then('the product page is shown', function () {
    expect(AmazonPage.productTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the DataDive Profitability Calculator button is enabled', function () {
    expect(DataDiveProfitsPage.dataDiveButton).toExist();
  });

  When('I click the DataDive Profitability Calculator button', function () {
    DataDiveProfitsPage.dataDiveButton.click();
  });  
    
  Then('the DataDive Mechanical Gaming Keyboard Profits Page is visible', function () {
    expect(browser).toHaveLinkContaining(DataDiveProfitsPage.DataDiveProfitsLink);
    expect(DataDiveProfitsPage.dataDiveProductTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the Your Strategy section is visible'), function () {
    expect(DataDiveProfitsPage.yourStrategyOpen).toHaveValueContaining('true');
  };

  When('I enter 100 in the monthly sales velocity textbox'), function () {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(100);
  };

  When('I click on the Product Design section'), function () {
    DataDiveProfitsPage.productDesignButton.click();
  };

  Then('the Product Design section is visible'), function () {
    expect(DataDiveProfitsPage.productDesignOpen).toHaveValueContaining('true');
  };

  Then('the Referral fee text has a value of {referral_fee} for the default category with minimum of {min}'), function (referral_fee, min) {
    expect(DataDiveProfitsPage.referralFeeDescription).toHaveTextContaining(`${referral_fee}, minimum ${min}`);
  };

  Then('the Product Gross Profit for Best shipping method has a value of {profit}'), function (profit) {
    expect(DataDiveProfitsPage.bestShippingProfit).toHaveTextContaining(profit);
  };

  Then('the Product Weight has a value of {pw}'), function (pw) {
    expect(DataDiveProfitsPage.productWeightFieldValue).toHaveTextContaining(pw);
  };

  Then('the sum of {price} and {fba_fee} times the {referral_fee} is lower than or equal to {min}'), function (price, fba_fee, referral_fee, min) {
    let sum = parseFloat(price.substring(1)) + parseFloat(fba_fee.substring(1));
    let ref_fee = parseInt(referral_fee.substring(0,referral_fee.length-1)) / 100;
    let total = sum * ref_fee;
    expect(total <= min);
  };

  When('I clear the Product Weight textbox'), function () {
    DataDiveProfitsPage.productWeightField.setValue(null);
  };

  When('I enter {pw2} in the Product Weight textbox'), function (pw2) {
    DataDiveProfitsPage.productWeightField.setValue(pw2);
  };

  Then('value of Product Gross Profit for Best shipping method must remain {profit}'), function (profit) {
    expect(DataDiveProfitsPage.bestShippingProfit).toHaveTextContaining(profit);
  };


// 3) Scenario: Calculate referral fee percentage change within category # features\category_referral_fee.feature:58

  Given('I am on the website of Amazon', function () {
    AmazonPage.open();
    browser.maximizeWindow();
    expect(browser).toHaveTitleContaining('Amazon');
  });

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter Mechanical Gaming Keyboard in the search bar', function () {
    AmazonPage.searchBar.setValue('Mechanical Gaming Keyboard');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for Mechanical Gaming Keyboard is shown', function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I click on the first result', function () {
    AmazonPage.firstResultLink.click();
  });

  Then('the product page is shown', function () {
    expect(AmazonPage.productTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the DataDive Profitability Calculator button is enabled', function () {
    expect(DataDiveProfitsPage.dataDiveButton).toExist();
  });

  When('I click the DataDive Profitability Calculator button', function () {
    DataDiveProfitsPage.dataDiveButton.click();
  });  
    
  Then('the DataDive Mechanical Gaming Keyboard Profits Page is visible', function () {
    expect(browser).toHaveLinkContaining(DataDiveProfitsPage.DataDiveProfitsLink);
    expect(DataDiveProfitsPage.dataDiveProductTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the Your Strategy section is visible'), function () {
    expect(DataDiveProfitsPage.yourStrategyOpen).toHaveValueContaining('true');
  };

  When('I enter 100 in the monthly sales velocity textbox'), function () {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(100);
  };

  When('I click on the Product Design section'), function () {
    DataDiveProfitsPage.productDesignButton.click();
  };

  Then('the Product Design section is visible'), function () {
    expect(DataDiveProfitsPage.productDesignOpen).toHaveValueContaining('true');
  };

  Then('the selected category has a referral fee text of {referral_fee_1} up to {min}, {referral_fee_2} otherwise'), function (referral_fee_1, min, referral_fee_2) {
    expect(DataDiveProfitsPage.referralFeeDescription).toHaveTextContaining(`${referral_fee_1} if price is up to ${min}, ${referral_fee_2} otherwise`);
  };

  Then('the Product Gross Profit for Best shipping method has a value of {profit}'), function (profit) {
    expect(DataDiveProfitsPage.bestShippingProfit).toHaveTextContaining(profit);
  };
  
  Then('the Product Weight has a value of {pw}'), function (pw) {
    expect(DataDiveProfitsPage.productWeightFieldValue).toHaveTextContaining(pw);
  };

  Then('the sum of {price} and {fba_fee} times the {referral_fee} is equal to {min}'), function (price, fba_fee, referral_fee, min) {
    let sum = parseFloat(price.substring(1)) + parseFloat(fba_fee.substring(1));
    let ref_fee = parseInt(referral_fee.substring(0,referral_fee.length-1)) / 100;
    let total = sum * ref_fee;
    expect(total == min);
  };

  When('I clear the Product Weight textbox'), function () {
    DataDiveProfitsPage.productWeightField.setValue(null);
  };

  When('I enter {pw2} in the Product Weight textbox'), function (pw2) {
    DataDiveProfitsPage.productWeightField.setValue(pw2);
  };

  Then('value of Product Gross Profit for Best shipping method must be {profit2}'), function (profit2) {
    expect(DataDiveProfitsPage.bestShippingProfit).toHaveTextContaining(profit2);
  };
