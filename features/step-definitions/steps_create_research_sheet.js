const { Given, When, Then } = require('@cucumber/cucumber');
const AmazonPage = require('../pages/amazon.page');
const DataDiveProfitsPage = require('../pages/datadive-profits.page');
const HeliumPage = require('../pages/helium.page');
const GsheetPage = require('../pages/gsheet.page');
const moment= require('moment');


// 1) Scenario: Generate best seller sheet of "<product>" on Amazon # features\create_research_sheet.feature:18

  Given('I am on the website of Amazon'), function () {
    AmazonPage.open();
    browser.maximizeWindow();
    expect(browser).toHaveTitleContaining('Amazon');
  };

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter {product} in the search bar', function (product) {
    AmazonPage.searchBar.setValue(product);
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for {product} is shown', function (product) {
    expect(AmazonPage.resultsPage).toHaveTextContaining(product);
  });

  When('I click on the Helium10 extension'), function () {
    // Click Helium10
  };

  When('I click on the XRay option'), function () {
    // Click XRay
  };

  Then('a table with results of products with {product} in the title is shown'), function (product) {
    expect(HeliumPage.heliumTitle).toHaveTextContaining(product);
  };

  When('I double click the Sales column header'), function () {
    HeliumPage.salesHeader.doubleClick()
  };
 
  Then('the results are sorted by sales'), function () {
    expect(int(HeliumPage.salesFirstCell.trim()) >= int(HeliumPage.salesSecondCell.trim()));
  };

  When('I select {number} items with relevant title and image'), function (number) {
    HeliumPage.asinsCount.selectByAttribute('value', number);
  };

  When('I click on the Dive button'), function () {
    DataDiveProfitsPage.diveButton.click();
  };

  Then('a Gsheet file should be generated'), function () {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
  };

  Then('the generated file name should contain the date in YYMMDD format, {account_file_number} and {product}'), function (account_file_number, product) {
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(account_file_number);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);
  };

  Then('{number} products should be shown on the product table'), function (number) {
    // other extension to interact with gsheet?
  };


// 2) Scenario:  Generate best seller sheet of product on Amazon without a subscription # features\create_research_sheet.feature:39

  Given('I am on the website of Amazon'), function () {
    AmazonPage.open();
    browser.maximizeWindow();
    expect(browser).toHaveTitleContaining('Amazon');
  };

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter dog hat in the search bar', function () {
    AmazonPage.searchBar.setValue('dog hat');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for dog hat is shown'), function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('dog hat');
  };

  But('my DataDive subscription has ended'), function () {
    // Interact with extension
  };

  When('I click on the Helium10 extension'), function () {
    // Interact with extension
  };

  When('I click on the XRay option'), function () {
    // Interact with extension
  };

  Then('a table with results of products with dog hat in the title is shown'), function () {
    expect(HeliumPage.heliumTitle).toHaveTextContaining(product);
  };

  When('I select Research the top 10 results from the DataDive dropdown menu'), function () {
    DataDiveProfitsPage.asinsCount.selectByAttribute('value', 10);
  };

  When('I click on the Dive button'), function () {
    DataDiveProfitsPage.diveButton.click();
  };

  Then('a warning to activate a subscription to DataDive should be shown'), function () {
    expect(DataDiveProfitsPage.subscribeButton).toHaveText('Subscribe now')
  };
