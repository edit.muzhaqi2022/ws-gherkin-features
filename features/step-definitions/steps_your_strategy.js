const { Given, When, Then } = require('@cucumber/cucumber');
const AmazonPage = require('../pages/amazon.page');
const DataDiveProfitsPage = require('../pages/datadive-profits.page');


// 1) Scenario: Calculate products per order without monthly sales velocity # features\your_strategy.feature:23

  Given('I am on the website of Amazon', function () {
    AmazonPage.open();
    browser.maximizeWindow();
    expect(browser).toHaveTitleContaining('Amazon');
  });

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter Mechanical Gaming Keyboard in the search bar', function () {
    AmazonPage.searchBar.setValue('Mechanical Gaming Keyboard');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for Mechanical Gaming Keyboard is shown', function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I click on the first result', function () {
    AmazonPage.firstResultLink.click();
  });

  Then('the product page is shown', function () {
    expect(AmazonPage.productTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the DataDive Profitability Calculator button is enabled', function () {
    expect(DataDiveProfitsPage.dataDiveButton).toExist();
  });

  When('I click the DataDive Profitability Calculator button', function () {
    DataDiveProfitsPage.dataDiveButton.click();
  });  
    
  Then('I am on the DataDive Mechanical Gaming Keyboard Profits Page', function () {
    expect(browser).toHaveLinkContaining(DataDiveProfitsPage.DataDiveProfitsLink);
    expect(DataDiveProfitsPage.dataDiveProductTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I clear the Monthly sales velocity textbox value', function () {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(null);
  });

  Then('the Products per Order textbox should be populated with a value of —pcs\/order', function () {
    expect(DataDiveProfitsPage.productsPerOrder).toHaveText(' pcs/order');
  });

  Then('a warning to populate the Monthly sales velocity textbox should be shown', function () {
    expect(DataDiveProfitsPage.msvAlert).toExist()
  });


// 2) Scenario: Calculate products per order # features\your_strategy.feature:39

  Given('I am on the website of Amazon', function () {
    AmazonPage.open();
    browser.maximizeWindow()
    expect(browser).toHaveTitle('Amazon.com. Spend less. Smile more.');
  });

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter Mechanical Gaming Keyboard in the search bar', function () {
    AmazonPage.searchBar.setValue('Mechanical Gaming Keyboard');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for Mechanical Gaming Keyboard is shown', function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I click on the first result', function () {
    AmazonPage.firstResultLink.click();
  });

  Then('the product page is shown', function () {
    expect(AmazonPage.productTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the DataDive Profitability Calculator button is enabled', function () {
    expect(DataDiveProfitsPage.dataDiveButton).toExist();
  });

  When('I click the DataDive Profitability Calculator button', function () {
    DataDiveProfitsPage.dataDiveButton.click();
  }); 

  Then('I am on the DataDive Mechanical Gaming Keyboard Profits Page', function () {
    expect(browser).toHaveLinkContaining(DataDiveProfitsPage.DataDiveProfitsLink);
    expect(DataDiveProfitsPage.dataDiveProductTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I enter a value of {msv} in the Monthly sales velocity textbox', function (msv) {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(msv);
  });

  Then('the Products per Order textbox should be populated with a value of {ppo}', function (ppo) {
    expect(DataDiveProfitsPage.productsPerOrder).toHaveText(`${ppo} pcs/order`);
  });


// 3) Scenario: Calculate longer production time effect on products per order # features\your_strategy.feature:53

  Given('I am on the website of Amazon', function () {
    AmazonPage.open();
    browser.maximizeWindow()
    expect(browser).toHaveTitle('Amazon.com. Spend less. Smile more.');
  });

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter Mechanical Gaming Keyboard in the search bar', function () {
    AmazonPage.searchBar.setValue('Mechanical Gaming Keyboard');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for Mechanical Gaming Keyboard is shown', function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I click on the first result', function () {
    AmazonPage.firstResultLink.click();
  });

  Then('the product page is shown', function () {
    expect(AmazonPage.productTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the DataDive Profitability Calculator button is enabled', function () {
    expect(DataDiveProfitsPage.dataDiveButton).toExist();
  });

  When('I click the DataDive Profitability Calculator button', function () {
    DataDiveProfitsPage.dataDiveButton.click();
  }); 

  Then('I am on the DataDive Mechanical Gaming Keyboard Profits Page', function () {
    expect(browser).toHaveLinkContaining(DataDiveProfitsPage.DataDiveProfitsLink);
    expect(DataDiveProfitsPage.dataDiveProductTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the Production lead time field is populated with a value of 2 weeks', function () {
    expect(DataDiveProfitsPage.prodLeadTime).toHaveText('2 weeks');
  });

  When('I write a value of {msv} in the Monthly sales velocity textbox', function (msv) {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(msv);
  });

  Then('the Products per Order textbox is populated with a value of {ppo}', function (ppo) {
    expect(DataDiveProfitsPage.productsPerOrder).toHaveText(`${ppo} pcs/order`);
  });

  When('I select a Production lead time value of {week} weeks from the drop down menu', function (week) {
    DataDiveProfitsPage.prodLeadTimeMenu.selectByAttribute('value', week);
  });

  Then('the Products per Order textbox should be populated with a value of {ppo2}', function (ppo2) {
    expect(DataDiveProfitsPage.productsPerOrder).toHaveText(`${ppo2} pcs/order`);
  });


// 4) Scenario: Calculate higher monthly sales effect on products per order # features\your_strategy.feature:68

  Given('I am on the website of Amazon', function () {
    AmazonPage.open();
    browser.maximizeWindow()
    expect(browser).toHaveTitle('Amazon.com. Spend less. Smile more.');
  });

  When('I click on Deliver to menu', function () {
    AmazonPage.deliverToButton.click();
  });

  When('I enter “90017” in the my US zip code textbox', function () {
    AmazonPage.zipTextbox.setValue('90017');
  });

  When('I click the apply button', function () {
    AmazonPage.zipApplyButton.click();
  });

  When('I click the Done button', function () {
    AmazonPage.zipDoneButton.click();
  });

  Then('the Deliver menu to is changed to “Los Angeles 90017” in the Amazon Page', function () {
    expect(AmazonPage.deliverToText).toHaveTextContaining('Los Angeles 90017');
  });

  When('I enter Mechanical Gaming Keyboard in the search bar', function () {
    AmazonPage.searchBar.setValue('Mechanical Gaming Keyboard');
  });

  When('I click on the Search button', function () {
    AmazonPage.searchButton.click();
  });

  Then('the results page for Mechanical Gaming Keyboard is shown', function () {
    expect(AmazonPage.resultsPage).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I click on the first result', function () {
    AmazonPage.firstResultLink.click();
  });

  Then('the product page is shown', function () {
    expect(AmazonPage.productTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  Then('the DataDive Profitability Calculator button is enabled', function () {
    expect(DataDiveProfitsPage.dataDiveButton).toExist();
  });

  When('I click the DataDive Profitability Calculator button', function () {
    DataDiveProfitsPage.dataDiveButton.click();
  }); 

  Then('I am on the DataDive Mechanical Gaming Keyboard Profits Page', function () {
    expect(browser).toHaveLinkContaining(DataDiveProfitsPage.DataDiveProfitsLink);
    expect(DataDiveProfitsPage.dataDiveProductTitle).toHaveTextContaining('Mechanical Gaming Keyboard');
  });

  When('I write a value of {msv} in the Monthly sales velocity textbox', function (msv) {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(msv);
  });

  Then('the Products per Order textbox is populated with a value of {ppo}', function (ppo) {
    expect(DataDiveProfitsPage.productsPerOrder).toHaveText(`${ppo} pcs/order`);
  });

  When('I write a value of {msv2} in the Monthly sales velocity textbox', function (msv2) {
    DataDiveProfitsPage.monthlySalesTextbox.setValue(msv2);
  });

  Then('the Products per Order textbox should be populated with a value of {ppo2}', function (ppo2) {
    expect(DataDiveProfitsPage.productsPerOrder).toHaveText(`${ppo2} pcs/order`);
  });
