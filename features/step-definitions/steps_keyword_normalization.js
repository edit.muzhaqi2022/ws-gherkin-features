const { Given, When, Then } = require('@cucumber/cucumber');
const GsheetPage = require('../pages/gsheet.page');
const moment= require('moment');


// 1) Scenario: Reset button empties PPC Keywords sheet # features\keyword_normalization.feature:15

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  When('I scroll the tabs till PPC Keywords is visible'), function () {
    
  };

  When('I click on the PPC Keywords tab'), function () {
    
  };

  Then('the PPC Keywords sheet is visible'), function () {
    
  };

  When('I click on the Import MKL button'), function () {
    
  };

  
  Then('the PPC Keywords sheet is filled'), function () {
    
  };

  When('I click on the Reset button'), function () {
    
  };

  Then('all the data cells should be blank'), function () {
    
  };

  Then('the headers should remain'), function () {
    
  };



// 2) Scenario: Include in BROAD campaign? checkbox generates keyword # features\keyword_normalization.feature:28

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  When('I scroll the tabs till PPC Keywords is visible'), function () {
    
  };

  When('I click on the PPC Keywords tab'), function () {
    
  };

  Then('the PPC Keywords sheet is visible'), function () {
    
  };

  When('I click on the Import MKL button'), function () {
    
  };

  Then('the PPC Keywords sheet is filled'), function () {

  };

  Then('a checkbox under Include in BROAD campaign? is unchecked'), function () {

  };

  When('I check the Include in BROAD campaign? checkbox'), function () {

  };

  Then('a string should be generated in the BROAD campaign keywords column on the same row'), function () {

  };


// 3) Scenario: Include in BROAD campaign? checkbox clears column # features\keyword_normalization.feature:41

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  When('I scroll the tabs till PPC Keywords is visible'), function () {
    
  };

  When('I click on the PPC Keywords tab'), function () {
    
  };

  Then('the PPC Keywords sheet is visible'), function () {
    
  };

  When('I click on the Import MKL button'), function () {
    
  };

  Then('the PPC Keywords sheet is filled'), function () {

  };

  Then('a checkbox under Include in BROAD campaign? is checked'), function () {

  };

  Then('column U is expanded'), function () {

  };

  When('I uncheck the Include in BROAD campaign? checkbox'), function () {

  };

  Then('all data in the BROAD campaign columns T, U, V, W must be blank'), function () {

  };


// 4) Scenario: Campaign negative keywords generation # features\keyword_normalization.feature:55

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  When('I scroll the tabs till PPC Keywords is visible'), function () {
    
  };

  When('I click on the PPC Keywords tab'), function () {
    
  };

  Then('the PPC Keywords sheet is visible'), function () {
    
  };

  When('I click on the Import MKL button'), function () {
    
  };
 
  Then('the PPC Keywords sheet is filled'), function () {

  };

  When('I expand column Q'), function () {

  };

  When('I expand column X'), function () {

  };

  When('I select a Normalized Keyword {kw}'), function (kw) {

  };

  When('I check the Include in EXACT campaign? checkbox on the same row'), function () {

  };

  When('I check the Include in PHRASE campaign? checkbox on the same row'), function () {

  };

  Then('{kw} should appear on PHRASE campaign negatives column R'), function (kw) {

  };

  Then('{kw} should appear on AUTO campaign EXACT negatives column Y'), function (kw) {

  };

  Then('{kw} should appear on AUTO campaign PHRASE negatives column Z'), function (kw) {

  };


// 5) Scenario: Include in BROAD campaign? checkbox creates negative keywords # features\keyword_normalization.feature:15

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  When('I scroll the tabs till PPC Keywords is visible'), function () {
    
  };

  When('I click on the PPC Keywords tab'), function () {
    
  };

  Then('the PPC Keywords sheet is visible'), function () {
    
  };

  When('I click on the Import MKL button'), function () {
    
  };
  
  Then('the PPC Keywords sheet is filled'), function () {
    
  };

  When('I expand column X'), function () {
    
  };

  When('I select a Normalized Keyword {kw}'), function (kw) {

  };

  When('I uncheck the corresponding Include in BROAD campaign? checkbox on the same row'), function () {
    
  };

  When('I check the Include in BROAD campaign? checkbox on the same row'), function () {
    
  };

  Then('{kw} should appear on AUTO campaign EXACT negatives column Y'), function (kw) {

  };

  Then('{kw} should appear on AUTO campaign PHRASE negatives column Z'), function (kw) {

  };
