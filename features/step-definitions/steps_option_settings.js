const { Given, When, Then } = require('@cucumber/cucumber');
const moment= require('moment');


// 1) Scenario: Lowering Maximum Relevant Rank effects Relevancy # features\your_strategy.feature:12

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  Then('the Master KW List is filled'), function () {

  };

  Given('the first keyword row has a Relevancy of {rlv}'), function (rlv) {

  };

  When('I scroll the tabs till Settings is visible'), function () {

  };

  When('I click on the Settings tab'), function () {

  };

  Then('the Settings sheet is filled'), function () {

  };

  Then('the Maximum Relevant Rank has a value of {mrr}'), function (mrr) {

  };

  When('I clear the Maximum Relevant Rank cell'), function () {

  };

  When('I enter a value of {mrr2} to the Maximum Relevant Rank cell'), function (mrr2) {

  };

  When('I click on the CLICK HERE TO APPLY settings and Master KW List filters button'), function () {

  };

  When('I scroll the tabs till Master KW List is visible'), function () {

  };

  When('I click on the Master KW List tab'), function () {

  };

  Then('Relevancy of the first keyword row must be {rlv2}'), function (rlv2) {

  };


// 2) Scenario: Green - Yellow Treshold Percentage change # features\your_strategy.feature:32

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  Then('the Master KW List is filled'), function () {

  };

  Given('the third product has a % of SV on P1 value of {green} and is colored green'), function (green) {

  };

  When('I scroll the tabs till Settings is visible'), function () {

  };

  When('I click on the Settings tab'), function () {

  };

  Then('the Settings sheet is filled'), function () {

  };

  Then('Maximum Competitor Quality treshold percentage for green has a value of {green}'), function (green) {

  };

  When('I clear the Maximum Competitor Quality for green treshold percentage for green cell'), function () {

  };

  When('I enter a value of {green2} to the Maximum Competitor Quality treshold percentage for green'), function (green2) {

  };

  When('I click on the CLICK HERE TO APPLY settings and Master KW List filters button'), function () {

  };

  When('I scroll the tabs till Master KW List is visible'), function () {

  };

  When('I click on the Master KW List tab'), function () {

  };

  Then('the third product should be colored yellow'), function () {

  };


// 3) Scenario: Orange - Red Treshold Percentage change # features\your_strategy.feature:52

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  Then('the Master KW List is filled'), function () {

  };

  Given('the first product has a % of SV on P1 value of {orange2} and is colored red'), function (orange2) {

  };

  When('I scroll the tabs till Settings is visible'), function () {

  };

  When('I click on the Settings tab'), function () {

  };

  Then('the Settings sheet is filled'), function () {

  };

  Then('Maximum Competitor Quality treshold percentage for orange has a value of {orange}'), function (orange) {

  };

  When('I clear the Maximum Competitor Quality treshold percentage for orange'), function () {

  };

  When('I enter a value of {orange2} to the Maximum Competitor Quality treshold percentage for orange'), function (orange2) {

  };

  When('I click on the CLICK HERE TO APPLY settings and Master KW List filters button'), function () {

  };

  When('I scroll the tabs till Master KW List is visible'), function () {

  };

  When('I click on the Master KW List tab'), function () {

  };

  Then('the first product should be colored orange'), function () {

  };


// 4) Scenario: Same Treshold Percentage effect # features\your_strategy.feature:72

  Given('I am on the DataDive generated research sheet for {product}'), function (product) {
    expect(browser).toHaveLinkContaining('https://docs.google.com/spreadsheets/d/');
    let YYMMDD = moment(moment().toDate()).format('YYMMDD');
    expect(GsheetPage.fileTitle).toHaveTextContaining(YYMMDD);
    expect(GsheetPage.fileTitle).toHaveTextContaining(product);    
  };

  Then('the Master KW List is filled'), function () {

  };
  
  Given('there are products colored yellow'), function () {

  };

  When('I scroll the tabs till Settings is visible'), function () {

  };

  When('I click on the Settings tab'), function () {

  };

  Then('Maximum Competitor Quality treshold percentage for yellow has a value of {yellow}'), function (yellow) {

  };

  Then('Maximum Competitor Quality treshold percentage for green has a value of {green}'), function (green) {

  };

  When('I clear the Maximum Competitor Quality treshold percentage for yellow'), function () {

  };

  When('I enter a value of {green} to the Maximum Competitor Quality treshold percentage for yellow'), function (green) {

  };

  When('I click on the CLICK HERE TO APPLY settings and Master KW List filters button'), function () {

  };

  When('I scroll the tabs till Master KW List is visible'), function () {

  };

  When('I click on the Master KW List tab'), function () {

  };

  Then('none of the products should be colored yellow'), function () {

  };
