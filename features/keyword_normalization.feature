Feature: Keyword Normalization
  Use Keyword Normalization feature in DataDive Research Sheet for PPC Search optimization


# Basic start background scenario
  Background:
    Given I am on the DataDive generated research sheet for "<product>"
    When I scroll the tabs till PPC Keywords is visible
    And I click on the PPC Keywords tab
    Then the PPC Keywords sheet is visible
    When I click on the Import MKL button


# Reset Button Test
  Scenario Outline: Reset button empties PPC Keywords sheet
    Then the PPC Keywords sheet is filled
    When I click on the Reset button
    Then all the data cells should be blank
    But the headers should remain
    
  Examples:
    | product                       |
    | Mechanical Gaming Keyboard    |
    | Wired Gaming Mouse            |


# Scenario to check Include in campaigns? checkboxes work  
  Scenario Outline: Include in BROAD campaign? checkbox generates keyword
    Then the PPC Keywords sheet is filled
    And a checkbox under Include in BROAD campaign? is unchecked
    When I check the Include in BROAD campaign? checkbox
    Then a string should be generated in the BROAD campaign keywords column on the same row
    
  Examples:
    | product                       |
    | Mechanical Gaming Keyboard    |
    | Wired Gaming Mouse            |


# Scenario to check Include in campaigns? checkboxes work 2
  Scenario Outline: Include in BROAD campaign? checkbox clears column
    Then the PPC Keywords sheet is filled
    And a checkbox under Include in BROAD campaign? is checked
    * column U is expanded
    When I uncheck the Include in BROAD campaign? checkbox
    Then all data in the BROAD campaign columns T, U, V, W must be blank
    
  Examples:
    | product                       |
    | Mechanical Gaming Keyboard    |
    | Wired Gaming Mouse            |


# Scenario to test Negative keyword columns
  Scenario Outline: Campaign negative keywords generation
    Then the PPC Keywords sheet is filled
    When I expand column Q
    And I expand column X
    And I select a Normalized Keyword "<kw>"
    And I check the Include in EXACT campaign? checkbox on the same row
    And I check the Include in PHRASE campaign? checkbox on the same row
    Then "<kw>" should appear on PHRASE campaign negatives column R
    And "<kw>" should appear on AUTO campaign EXACT negatives column Y
    And "<kw>" should appear on AUTO campaign PHRASE negatives column Z
    
  Examples:
    | product                       | kw              |
    | Mechanical Gaming Keyboard    | keyboard gaming |
    | Wired Gaming Mouse            | wired mouse     |
    

# Scenario to test Negative keyword columns 2
  Scenario Outline: Include in BROAD campaign? checkbox creates negative keywords
    Then the PPC Keywords sheet is filled
    When I expand column X
    And I select a Normalized Keyword "<kw>"
    And I uncheck the corresponding Include in BROAD campaign? checkbox on the same row
    And I check the Include in BROAD campaign? checkbox on the same row
    Then "<kw>" should appear on AUTO campaign EXACT negatives column Y
    And "<kw>" should appear on AUTO campaign PHRASE negatives column Z

    
  Examples:
    | product                       | kw              |
    | Mechanical Gaming Keyboard    | keyboard gaming |
    | Wired Gaming Mouse            | wired mouse     |
